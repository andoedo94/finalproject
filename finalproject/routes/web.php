<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['guest'])->group(function () {

    Route::get('/', [IndexController::class, 'Index']);
    Route::post('/registro', [IndexController::class, 'Registro']);
    Route::get('/login', [IndexController::class, 'Login'])->name("login");
    Route::post('/loguear', [IndexController::class, 'loguear']);

});

Route::middleware(['auth'])->group(function () {

    Route::get('/home', [IndexController::class, 'Home']);
    Route::get('/logout', function () {
        \Auth::logout();
        return redirect('/');
    });

    Route::get('/download', [HomeController::class, 'Download']);
    Route::get('/encriptar', [HomeController::class, 'Encriptar']);
    Route::post('/encrypt', [HomeController::class, 'Encrypt']);
    Route::get('/desencriptar', [HomeController::class, 'Desencriptar']);
    Route::post('/decrypt', [HomeController::class, 'Decrypt']);
    
});




