<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\FileRequest;

class HomeController extends Controller
{
    public function Download(){
        $file="Username;Nombre;Apellido;Password\r\n";
        $users=User::all();
        foreach($users as $user){
            $file=$file.$user->username.";".$user->nombre.";".$user->apellido.";".$user->password."\r\n";
        }   

        \Storage::disk('local')->put('users.txt', $file);
        return \Storage::disk('local')->download('users.txt');
    }

    public function Encriptar(){
        return view('index.encriptar');
    }

    public function Encrypt(FileRequest $request){
        $contenido = file_get_contents($request->file->getRealPath());       
        $data = $this->DoEncrypt($contenido, $request->key);
        \Storage::disk('local')->put('encriptado.txt', $data);
        return \Storage::disk('local')->download('encriptado.txt');

    }

    public function Desencriptar(){
        return view('index.desencriptar');
    }

    public function Decrypt(FileRequest $request){
        $contenido = file_get_contents($request->file->getRealPath());
        $data = $this->DoDecrypt($contenido, $request->key);
        \Storage::disk('local')->put('desencriptado.txt', $data);
        return \Storage::disk('local')->download('desencriptado.txt');

    }

    private function DoEncrypt($contenido, $key){
        $method = "AES-256-CBC";
        $option = 0;
        $iv = str_repeat("0", openssl_cipher_iv_length($method));
        $encrypted = openssl_encrypt($contenido, $method, $key, $option, $iv);
        return $encrypted;
    }

    private function DoDecrypt($contenido, $key){
        $method = "AES-256-CBC";
        $option = 0;
        $iv = str_repeat("0", openssl_cipher_iv_length($method));
        return openssl_decrypt($contenido, $method, $key, $option, $iv);
    }
}
