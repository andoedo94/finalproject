<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Models\User;

class IndexController extends Controller
{
    public function Index(){
        return view('index.index');
    }

    public function Registro(RegisterRequest $request){

        $user = new User();
        $user->username = $request->username;
        $user->nombre = $request->nombre;
        $user->apellido = $request->apellido;
        $user->password = sha1($request->password);
        $user->save();
        \Session::flash("mensaje", "Usuario creado exitosamente");
        return redirect("/login");
    }

    public function Login(){
        return view('index.login');
    }

    public function Loguear(LoginRequest $request){
        $user=User::where('username', $request->username)->first();
        if($user != null){

            if($user->password == sha1($request->password))
            {
                \Auth::login($user);
                return redirect("/home");
            }

            \Session::flash("error", "Usuario y/o contraseña invalidos");
            return redirect("/login")->withInput($request->except(['password']));
        }

        \Session::flash("error", "Usuario y/o contraseña invalidos");
        return redirect("/login")->withInput($request->except(['password']));
    }

    public function Home(){
        return view('index.home');
    }
}
