@extends('layout.layout')
 
@section('content')
    
   <h2 class="text-center">Registro</h2>
   <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{Form::open(["method"=>"post", "url" => "/registro"])}}

            <div class="mb-3">
                <label  class="form-label">Nombre de Usuario</label>
                {{Form::text('username', null, ["class"=>"form-control", "placeholder"=>"Nombre de usuario"])}}
            </div>

            <div class="mb-3">
                <label  class="form-label">Nombre</label>
                {{Form::text('nombre', null, ["class"=>"form-control", "placeholder"=>"Nombre"])}}
            </div>

            <div class="mb-3">
                <label  class="form-label">Apellido</label>
                {{Form::text('apellido', null, ["class"=>"form-control", "placeholder"=>"Apellido"])}}
            </div>

            <div class="mb-3">
                <label  class="form-label">Contraseña</label>
                {{Form::password('password',["class"=>"form-control", "placeholder"=>"Contraseña"])}}
            </div>

            <div class="mb-3">
                <input class="btn btn-primary" type="submit" value="Registrar">
            </div>           


        {{Form::close()}}
    </div>
    <div class="col-md-2"></div>
   </div>

@endsection