@extends('layout.layout')
 
@section('content')
    
   <h2 class="text-center">Desencriptar AES</h2>
   <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(\Session::get('mensaje')!= null)
            <div class="alert alert-success">
                {{\Session::get('mensaje')}}
            </div>
        @endif
        {{Form::open(["method"=>"post", "url" => "/decrypt", "files"=>true])}}

            <div class="mb-3">
                <label class="form-label">Archivo a desencriptar</label>
                {{Form::file('file', ["class"=>"form-control"])}}
                <span class="help-block">Se admiten solo archivos .txt</span>
            </div>

            <div class="mb-3">
                <label  class="form-label">Key</label>
                {{Form::text('key', null, ["class"=>"form-control", "placeholder"=>"key"])}}
            </div>

            <div class="mb-3">
                <input class="btn btn-primary" type="submit" value="Desencriptar">
            </div>           


        {{Form::close()}}
    </div>
    <div class="col-md-2"></div>
   </div>


@endsection