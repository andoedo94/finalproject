@extends('layout.layout')
 
@section('content')
    
   <h2 class="text-center">Login</h2>
   <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(\Session::get('mensaje')!= null)
            <div class="alert alert-success">
                {{\Session::get('mensaje')}}
            </div>
        @endif
        @if(\Session::get('error')!= null)
            <div class="alert alert-danger">
                {{\Session::get('error')}}
            </div>
        @endif
        {{Form::open(["method"=>"post", "url" => "/loguear"])}}

            <div class="mb-3">
                <label  class="form-label">Nombre de Usuario</label>
                {{Form::text('username', null, ["class"=>"form-control", "placeholder"=>"Nombre de usuario"])}}
            </div>

            <div class="mb-3">
                <label  class="form-label">Contraseña</label>
                {{Form::password('password',["class"=>"form-control", "placeholder"=>"Contraseña"])}}
            </div>

            <div class="mb-3">
                <input class="btn btn-primary" type="submit" value="Login">
            </div>           


        {{Form::close()}}
    </div>
    <div class="col-md-2"></div>
   </div>

@endsection